Experimental Bitcoin Wallet UI implementation.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Development
Docker is recommended.

```
git clone git@gitlab.com:kannapoix/bitcoin-wallet-ui.git
cd bitcoin-wallet-ui
sudo docker build . -t wallet-ui
sudo docker run --rm -it -p 3000:3000 --mount type=bind,source=$PWD,target=/usr/src/app wallet-ui sh -c 'cd ui && yarn install'
sudo docker run --rm -it -p 3000:3000 --mount type=bind,source=$PWD,target=/usr/src/app wallet-ui sh -c 'cd ui && yarn start'
```

Then access localhost:3000 via browser.